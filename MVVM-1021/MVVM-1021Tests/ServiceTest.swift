//
//  ServiceTest.swift
//  MVVM-1021Tests
//
//  Created by Anand Yadav on 13/10/21.
//

import XCTest
@testable import MVVM_1021

class ServiceTest: XCTestCase {

    func testGetPostService() {
        let expectation = expectation(description: "Expecting a Post Json not nil")
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            ServiceManager.getPosts(success: { (response) in
                XCTAssertTrue(response  is [Post])
                expectation.fulfill()
            }) { (error) in
                if let error = error {
                    XCTFail("error: \(error)")
                }
            }
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    
    func testGetPostByIDService() {
        let expectation = expectation(description: "Expecting a Post Json not nil")
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            ServiceManager.getPostWithId(postId: 2, success: { response in
                XCTAssertTrue(response  is [Post])
                expectation.fulfill()
            }, failure: { error in
                if let error = error {
                    XCTFail("error: \(error)")
                }
            })
        })
        waitForExpectations(timeout: 15, handler: nil)
    }

}
