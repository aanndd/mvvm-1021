//
//  AppExtension.swift
//  MVVM-1021
//
//  Created by Anand Yadav on 09/10/21.
//

import UIKit

extension Decodable {
    static func map(JSONData:Data) -> Self? {
        debugPrint(JSONData)

        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let utf8Data = String(decoding: JSONData, as: UTF8.self).data(using: .utf8)
            return try decoder.decode(Self.self, from: utf8Data!)
        } catch let error {
            print(error)
            return nil
        }
    }
}
