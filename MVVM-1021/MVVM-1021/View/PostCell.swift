//
//  PostCell.swift
//  MVVM-1021
//
//  Created by Anand Yadav on 09/10/21.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBody: UILabel!
    
    var post: Post?{
        didSet{
            postConfiguration()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func postConfiguration(){
        guard let id = post?.id else {
            return
        }
        lblId.text = String(id)
        lblTitle.text = post?.title
        lblBody.text = post?.body
    }

}
