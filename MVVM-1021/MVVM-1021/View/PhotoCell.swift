//
//  PhotoCell.swift
//  MVVM-1021
//
//  Created by Anand Yadav on 11/10/21.
//

import UIKit
import Kingfisher

class PhotoCell: UITableViewCell {

    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgViewThumbnail: UIImageView!
    
    var photo: Photo?{
        didSet{
            configurePhotoCell()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configurePhotoCell(){
        guard let id = photo?.id else {
            return
        }
        lblID.text = String(id)
        lblTitle.text = photo?.title
        imgViewThumbnail.kf.setImage(with: URL(string:(photo?.thumbnailUrl)!))
    }

}
