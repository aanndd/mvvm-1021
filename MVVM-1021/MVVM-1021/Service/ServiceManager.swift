//
//  ServiceManaher.swift
//  MVVM-1021
//
//  Created by Anand Yadav on 01/10/21.
//

import UIKit

class ServiceManager: NSObject {
    
    static func getComments(success: @escaping (_ response: Any?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getComments, success: { (response) in
            let comments = [Comment].map(JSONData:response as! Data)!
            success(comments)
        }) { (error) in
            failure(error)
        }
    }
    
    static func getAlbums(success: @escaping (_ response: Any?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getAlbum, success: { (response) in
            let comments = [Album].map(JSONData:response as! Data)!
            success(comments)
        }) { (error) in
            failure(error)
        }
    }
    
    static func getPosts(success: @escaping (_ response: Any?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getPosts, success: { (response) in
            let posts = [Post].map(JSONData:response as! Data)!
            success(posts)
        }) { (error) in
            failure(error)
        }
    }
    
    static func getPhotos(success: @escaping (_ response: Any?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getPhotos, success: { (response) in
            let photos = [Photo].map(JSONData:response as! Data)!
            success(photos)
        }) { (error) in
            failure(error)
        }
    }
    
    static func getPostWithId(postId: Int, success: @escaping (_ response: Any?) -> Void,
                            failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getPostsByID(parameters: ["ID":postId]), success: { (response) in
            let photos = [Post].map(JSONData:response as! Data)!
            success(photos)
        }) { (error) in
            failure(error)
        }
    }
}

