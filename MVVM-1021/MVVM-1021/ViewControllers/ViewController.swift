//
//  ViewController.swift
//  MVVM-1021
//
//  Created by Anand Yadav on 01/10/21.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    var selectedSegmentIndex = 0
    var viewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.vc = self
        viewModel.getAllPosts()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func segmentSelected(_ sender: Any) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            viewModel.getAllPosts()
        case 2:
            viewModel.getAllPhotos()
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        selectedSegmentIndex = self.segment.selectedSegmentIndex
        switch self.segment.selectedSegmentIndex {
        case 0:
            return viewModel.post.count
        case 2:
            return viewModel.photos.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch selectedSegmentIndex {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "post") as! PostCell
            cell.post = viewModel.post[indexPath.row]
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "photo") as! PhotoCell
            cell.photo = viewModel.photos[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


