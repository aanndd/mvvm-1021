//
//  ViewModel.swift
//  MVVM-1021
//
//  Created by Anand Yadav on 10/10/21.
//

import UIKit

class ViewModel {
    weak var vc:ViewController?
    var post = [Post]()
    var photos = [Photo]()

    func getAllPosts() {
        ServiceManager.getPosts(success: { (response) in
            ProgressHUD.dismiss()
            ProgressHUD.showSucceed()
            self.post = response as! [Post]
            DispatchQueue.main.async{
                self.vc?.tableView.reloadData()
            }
        }) { (error) in
            ProgressHUD.dismiss()
            ProgressHUD.showFailed()
            print(error?.localizedDescription)
        }
    }
    
    func getAllPhotos() {
        ServiceManager.getPhotos(success: { (response) in
            ProgressHUD.dismiss()
            ProgressHUD.showSucceed()
            self.photos = response as! [Photo]
            DispatchQueue.main.async{
                self.vc?.tableView.reloadData()
            }
        }) { (error) in
            ProgressHUD.dismiss()
            ProgressHUD.showFailed()
            print(error?.localizedDescription)
        }
    }
}
